const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir.config.assetsPath = 'src';
elixir.config.publicPath = 'dist';

elixir.config.browserlist = {
    browsers: [ '> 1%', 'last 2 versions', 'Firefox >= 20', 'Opera 12.1', 'IE >= 9']
};

elixir(function (mix) {

    /*
     |--------------------------------------------------------------------------
     | Build scss files
     |--------------------------------------------------------------------------
     */
    mix.sass('app.scss');

    /*
     |--------------------------------------------------------------------------
     | Build JS files
     |--------------------------------------------------------------------------
     */
    mix.webpack('app.js', 'dist/js/app.js');
    mix.scripts([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/vue-resource/dist/vue-resource.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        './src/js/config.js',
        './src/js/storage.js',
        './src/js/recorder.js',
        './src/js/player.js'
    ], 'dist/js/main.js');

    /*
     |--------------------------------------------------------------------------
     | Copy fonts and other files
     |--------------------------------------------------------------------------
     */
    mix.copy([
        'node_modules/bootstrap-sass/assets/fonts',
        'node_modules/font-awesome/fonts',
    ], 'dist/fonts');

    mix.copy(['src/images'], 'dist/images');
    mix.copy(['src/songs'], 'dist/songs');
});

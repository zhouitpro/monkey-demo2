/**
 * Created by zhouitpro on 19/12/2016.
 */
'use strict';

import Vue from 'vue';

import VueResource from 'vue-resource'

import  VueRouter from 'vue-router'

import router from './router';


Vue.use(VueResource);
Vue.use(VueRouter);



Vue.http.interceptors.push((request, next) => {
    console.log('ENTERING ROUTE22');
    next();
});

const bus = new Vue({
    data: {
        title: "Monkey demo 2",
    }
});

window.bus = bus;



const app = new Vue({
    el: '#app',
    data: {
        msg: "hello",
    },
    computed: {},
    events: {},
    created(){
        console.log('Bootstrap.');
    },
    mounted(){
        console.log('Ready.');
    },
    methods:{

    },
    router,//加载路由（相当于router:router）,
//     watch: {
//     '$route' (to, from) {
//
//        // alert('main watch');
//         //console.log(this.$route.matched);
//         console.log(from);
//         if (from.matched[0].path == '/player/nid/:id'){
//              bus.$emit('flush_data');
//             alert('back!');
//
//         }
//
//
//     }
// }
});



var MonkeyConfig = (function() {
    "use strict";

    // var base_url = 'http://localhost:8888';
    var base_url = 'http://www.xiukun.me:8888';

    return {
        PlayListUrl: base_url + '/api/recordings?_format=hal_json',
        // BaseUrl: 'http://www.xiukun.me:8888/'
        BaseUrl: base_url+'/'
    }
})();

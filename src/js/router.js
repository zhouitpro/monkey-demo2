
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const main = require('./componets/main/main.vue');
const  player = require('./componets/player/player.vue');
const recorder = require('./componets/recorder/recorder.vue');
const recording_submit = require('./componets/recorder/submit.vue');

const routes = [
    { path: '/main', component: main, children:[
        {
            path:'recorder',
            component: recorder,
            children:[
                {
                    path:'submit',
                    component:recording_submit,
                    props: true
                }
            ]
        }
    ] },
    { path: '/player/nid/:id', component: player }
];


const router = new VueRouter({routes}); //这里可以带有路由器的配置参数

export default router; //将路由器导出

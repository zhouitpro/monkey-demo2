/**
 * @type {{getStorage, setStorage, remove, push, flush, getPlaylist, getPlaylistById}}
 */
var MonkeyStorage = (function ($) {

    // Define get json base url.
    var url = MonkeyConfig.PlayListUrl,
        indexName = 'playlist';

    return {
        getStorage: function () {
            return JSON.parse(localStorage.getItem(indexName));
        },
        setStorage: function (data) {
            return localStorage.setItem(indexName, JSON.stringify(data));
        },
        remove: function () {
            localStorage.removeItem(indexName);
        },
        push: function ($obj) {
            var $storeage = this.getStorage();
            $storeage.push($obj);
            this.setStorage($storeage);
        },
        flush: function () {
            $.ajax({
                url: url,
                type: 'GET',
                async: false,
                success: function (data) {
                    MonkeyStorage.setStorage(data);
                }
            });
            return this.getStorage();
        },
        getPlaylist: function () {
            if (this.getStorage() == 'undefined' || !this.getStorage()) {
                return this.flush();
            }
            return this.getStorage();
        },
        getPlaylistById: function ($playid) {
            if ($playid) {
                var $result = [];
                this.getPlaylist().forEach(function (v) {
                    $result[v.nid] = v;
                });

                if (typeof $result[$playid] !== 'undefined') {
                    return $result[$playid];
                }
            }
            console.log('Please set the correct playid.')
            return false;
        }
    }

})(jQuery);

